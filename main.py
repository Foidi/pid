#!/usr/bin/env pybricks-micropython

#!/usr/bin/env pybricks-micropython
import time
from pybricks.hubs import EV3Brick as brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import (Port, Stop, Direction, Button, Color)
                                  
from pybricks.tools import  wait, StopWatch
from pybricks.robotics import DriveBase

# Write your program here
# brick.light(Color)
#brick.sound.beep()
brick.screen.clear()
motorP = Motor(Port.A)
motorL = Motor(Port.D)
okoL = ColorSensor(Port.S4)
#okoS = ColorSensor(Port.S3)
okoP = ColorSensor(Port.S2)

error=0
error2=0
kp=1 #1.85
kd = 0 #1.85
ki = 0 #0.004
integral = 0
p=0
i=0
d=0
koniec=0
speed = 38
while True:
    ol=okoL.reflection()
    op=okoP.reflection()
    
    #tekst=str(ol)+" "+str(op)
    brick.screen.print(str(ol)+" "+str(op))
    #time.sleep(1)
    error = (ol-op)
    brick.screen.print("error="+str(error))
    p = int(error*kp)
    integral = (integral+error)
    i = int(integral*ki)
    d = (error-error2)*kd
    error2 = error		
    
    speedlewy=int(-speed-(p+i+d))
    speedprawy=int(speed-(p+i+d))
    if speedlewy<-100:
        speedlewy=-100
    else:
        if speedlewy>0:
            speedlewy=0
    if speedprawy>100:
        speedprawy=100
    else: 
        if speedprawy<0:
            speedprawy=0
    motorP.dc(speedprawy)
    motorL.dc(speedlewy)


